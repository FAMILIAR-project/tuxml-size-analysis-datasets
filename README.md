# tuxml-size-analysis-datasets

Datasets about size of Linux kernel configurations (TuxML project: https://github.com/TuxML) 

`dataset_encoded_all_size.csv` (4.13.3) has been obtained thanks to CSV here: https://github.com/TuxML/tuxml-datasets 
that have been encoded and merged with procedures located in `scripts413` 

The same story is almost the same for 4.15 (we extract data from database; we synthesize CSV; we merge CSVs; we encode data and we serialized as `pkl`)