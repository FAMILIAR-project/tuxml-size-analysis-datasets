import numpy as np
import pandas as pd

list_csv = [
        "config415_bdd20000-30000.csv",
    	"config415_bdd30000-40000.csv",	
        "config415_bdd50000-60000.csv",
        "config415_bdd40000-50000.csv",	
        "config415_bdd60000-70000.csv"
]

for csv in list_csv:

    df = pd.read_csv(csv)

    basic_head = ["cid"] # "compile"
    size_methods = ["vmlinux", "GZIP-bzImage", "GZIP-vmlinux", "GZIP", "BZIP2-bzImage", 
                  "BZIP2-vmlinux", "BZIP2", "LZMA-bzImage", "LZMA-vmlinux", "LZMA", "XZ-bzImage", "XZ-vmlinux", "XZ", 
                  "LZO-bzImage", "LZO-vmlinux", "LZO", "LZ4-bzImage", "LZ4-vmlinux", "LZ4"]
    compilation_status_column_name = 'kernel_size'

    # Filtering out X86_32 configuration
    # no need, anyway
    df.query("X86_64 == 'y'", inplace=True)

    tri_state_values = ['y', 'n', 'm']

    def same_encode(x):
        return str(x).replace("y","1").replace("n","0").replace("m","2")
    def encode_data_compilation(df):
        # we save quantitative values we want (here vmlinux, TODO: generalize)
        # the key idea is that the labelling encoder should not be applied to this kind of values (only to predictor variables!)
        # vml = rawtuxdata['LZO'] # rawtuxdata['vmlinux'] 
        o_sizes = df[size_methods]
        cid = df["cid"]
        # we may remove non tri state options, but TODO there are perhaps some interesting options (numerical or string) here
        #tuxdata = rawtuxdata.drop(columns=non_tristate_options).drop(columns=['vmlinux']).apply(le.fit_transform)
        df_encoded = df.drop(columns=['cid']).drop(columns=size_methods).applymap(same_encode)

        #tuxdata['vmlinux'] = vml 
        #df_encoded[size_methods] = o_sizes
        # we can ue vmlinux since it has been restored thanks to previous line
        df_encoded[size_methods] = o_sizes[size_methods]
        df_encoded["cid"] = cid
        return df_encoded

    df_encoded = encode_data_compilation(df)
    df_encoded.to_csv(csv.split(".")[0]+"_encoded_all_size.csv", index=False)