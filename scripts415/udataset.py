#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import json

with open("option_columns_415.json","r") as f:
    option_columns = json.load(f)

#Find the dataset
# df = pd.read_csv("dataset_encoded_size.csv", dtype={k:"int8" for k in option_columns})
df = pd.read_csv("dataset_encoded_all_size.csv", dtype={k:"int8" for k in option_columns})
# df.query("cid >= 30000", inplace=True)
df.fillna(-1, inplace=True)
df.query("vmlinux >= 0", inplace=True)

df.drop(columns=['Unnamed: 0'], inplace=True)
basic_head = ["cid"] # "compile"
size_methods = ["vmlinux", "GZIP-bzImage", "GZIP-vmlinux", "GZIP", "BZIP2-bzImage", 
              "BZIP2-vmlinux", "BZIP2", "LZMA-bzImage", "LZMA-vmlinux", "LZMA", "XZ-bzImage", "XZ-vmlinux", "XZ", 
              "LZO-bzImage", "LZO-vmlinux", "LZO", "LZ4-bzImage", "LZ4-vmlinux", "LZ4"]
df.drop_duplicates(subset=df.columns.difference(size_methods).difference(basic_head), inplace=True)

# In[2]:


df[:10]


# In[3]:


NO_ENCODED_VALUE = 0
YES_ENCODED_VALUE = 1
M_ENCODED_VALUE = 2

def nbyes(row):
    return sum(row == YES_ENCODED_VALUE)

def nbno(row):
    return sum(row == NO_ENCODED_VALUE)

def nbmodule(row):
    return sum(row == M_ENCODED_VALUE)

# df.query("kernel_size == 7304656")
# df.query("kernel_size == 7317008").apply(nbyes, axis=1) 
# df.query("vmlinux == 7317008").apply(nbyes, axis=1) 


# In[4]:


df['nbyes'] = df.apply(nbyes, axis=1)
df['nbno'] = df.apply(nbno, axis=1)
df['nbmodule'] = df.apply(nbmodule, axis=1)
df['nbyesmodule'] = df['nbyes'] + df['nbmodule']


# In[86]:


df.sort_values(by='vmlinux', ascending=True)[['vmlinux', 'nbno', 'nbyes', 'nbmodule', 'nbyesmodule']][:10]


# In[ ]:


df.to_pickle("all_size_withyes.pkl")

