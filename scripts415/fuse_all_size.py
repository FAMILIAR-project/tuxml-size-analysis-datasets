import pandas as pd


df1 = pd.read_csv("config415_bdd20000-30000_encoded_all_size.csv")
df2 = pd.read_csv("config415_bdd30000-40000_encoded_all_size.csv")
df3 = pd.read_csv("config415_bdd40000-50000_encoded_all_size.csv")
df4 = pd.read_csv("config415_bdd50000-60000_encoded_all_size.csv")
df5 = pd.read_csv("config415_bdd60000-70000_encoded_all_size.csv")

df = pd.concat([df1,df2,df3,df4,df5])

#Save some memory
del(df1)
del(df2)
del(df3)
del(df4)
del(df5)

print("Merged!", df.shape)

basic_head = ["cid"] # "compile"
size_methods = ["vmlinux", "GZIP-bzImage", "GZIP-vmlinux", "GZIP", "BZIP2-bzImage", 
              "BZIP2-vmlinux", "BZIP2", "LZMA-bzImage", "LZMA-vmlinux", "LZMA", "XZ-bzImage", "XZ-vmlinux", "XZ", 
              "LZO-bzImage", "LZO-vmlinux", "LZO", "LZ4-bzImage", "LZ4-vmlinux", "LZ4"]



print(str(len(df)) + " before the removal of some entries (those with same configurations)")
#Dropping duplicates
df.drop_duplicates(subset=df.columns.difference(size_methods).difference(basic_head), inplace=True)
print(str(len(df)) + " after the removal of some entries (those with same configurations)")

df.to_csv("dataset_encoded_all_size.csv", index=False)