#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import pandas as pd
import json

with open("option_columns.json","r") as f:
    option_columns = json.load(f)

#Find the dataset
# df = pd.read_csv("dataset_encoded_size.csv", dtype={k:"int8" for k in option_columns})
df = pd.read_csv("dataset_encoded_all_size.csv", dtype={k:"int8" for k in option_columns})
df.query("cid >= 30000", inplace=True)
df.fillna(-1, inplace=True)
df.query("vmlinux >= 0", inplace=True)

size_methods = ["vmlinux", "GZIP-bzImage", "GZIP-vmlinux", "GZIP", "BZIP2-bzImage", 
              "BZIP2-vmlinux", "BZIP2", "LZMA-bzImage", "LZMA-vmlinux", "LZMA", "XZ-bzImage", "XZ-vmlinux", "XZ", 
              "LZO-bzImage", "LZO-vmlinux", "LZO", "LZ4-bzImage", "LZ4-vmlinux", "LZ4"]

tri_state_values = [0, 1, 2]

ftuniques = []
non_tristate_options = []

for col in df:
    ft = df[col]    
    # eg always "y"
    if len(ft.unique()) == 1:
        ftuniques.append(col)
    # only tri-state values (y, n, m) (possible TODO: handle numerical/string options)    
    elif all(x in tri_state_values for x in ft.unique()):     #len(ft.unique()) == 3: 
        continue
    else:
        if not (col in size_methods): 
            non_tristate_options.append(col)
print("non_tristate_options:", len(non_tristate_options))
print("ftuniques", ftuniques)
df.drop(columns=ftuniques, inplace=True)


# In[ ]:


df[:10]


# In[ ]:


NO_ENCODED_VALUE = 0
YES_ENCODED_VALUE = 1
M_ENCODED_VALUE = 2

def nbyes(row):
    return sum(row == YES_ENCODED_VALUE)

def nbno(row):
    return sum(row == NO_ENCODED_VALUE)

def nbmodule(row):
    return sum(row == M_ENCODED_VALUE)

# df.query("kernel_size == 7304656")
# df.query("kernel_size == 7317008").apply(nbyes, axis=1) 
df.query("vmlinux == 7317008").apply(nbyes, axis=1) 


# In[ ]:


df['nbyes'] = df.apply(nbyes, axis=1)
df['nbno'] = df.apply(nbno, axis=1)
df['nbmodule'] = df.apply(nbmodule, axis=1)
df['nbyesmodule'] = df['nbyes'] + df['nbmodule']


# In[ ]:


df.sort_values(by='vmlinux', ascending=True)[['vmlinux', 'nbno', 'nbyes', 'nbmodule', 'nbyesmodule']][:10]


# In[ ]:


df.to_pickle("all_size_withyes.pkl")

